/*
 * main.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: jlenz
 */

#include <string>
#include <random>
#include <numeric>
#include <string>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <chrono>
using namespace std;

static mt19937 rng;

static void printGeneration(unsigned generationNumber, unsigned fitness,
                            string const & generation) {
  cout << "Gen: " << setw(5) << generationNumber << " | Fitness: " << setw(4)
       << fitness << " | " << generation << endl;
}

/// @brief Generates a fitness as the sum of the difference between matching characters
static unsigned fitness(string const & a, string const & b) {
  assert(a.length() == b.length());
  unsigned accum { 0 };
  for (auto aiter = a.begin(), biter = b.begin(); aiter != a.end();
      ++aiter, ++biter) {
    accum += abs(*aiter - *biter);
  }
  return accum;
}

/// @brief Generates a string of @a length random printable ASCII characters
static string randomString(size_t length) {
  uniform_int_distribution<> dist(' ', '~');
  string ret;
  while (length--) {
    ret.push_back(dist(rng));
  }
  return ret;
}

/// @brief Mutates a character based on an expected character delta from desired.
static char mutate(char mut, double delta) {
  assert(delta > 0);
  normal_distribution<> dist(0, delta * 2);
  char ret;
  do {
    double variate { round(dist(rng)) };
    ret = mut + variate;
  } while (!isprint(ret));
  return ret;
}

/// @brief Returns a mutated string given the string's fitness value.
static string mutate(string const & mut, double delta) {
  assert(delta > 0);
  std::string ret(mut);
  for (auto & it : ret) {
    // Assume each character is off by the fitness divided by the number of characters.
    it = mutate(it, delta / mut.length());
  }
  return ret;
}

/// @brief Evolves a provided population into the ideal population.
static void evolve(string const & firstGeneration,
                   string const & idealPopulation) {
  unsigned fit { fitness(firstGeneration, idealPopulation) };
  unsigned gen { 1 };
  printGeneration(gen, fit, firstGeneration);

  string previousGeneration { firstGeneration };

  while (fit != 0) {
    string nextGeneration { mutate(previousGeneration, fit) };
    unsigned nextFit { fitness(nextGeneration, idealPopulation) };
    ++gen;

    // Only accept the next generation if it is more fit than the last.
    if (nextFit < fit) {
      fit = nextFit;
      printGeneration(gen, fit, nextGeneration);
      previousGeneration = nextGeneration;
    }
  }
}

int main(int argc, char ** argv) {
  if (argc >= 2) {
    string idealString(argv[1]);
    string firstGeneration(randomString(idealString.length()));
    chrono::time_point<chrono::system_clock> start { chrono::system_clock::now() };
    evolve(firstGeneration, idealString);
    chrono::time_point<chrono::system_clock> end { chrono::system_clock::now() };
    cout
        << "Elapsed time is "
        << chrono::duration_cast<chrono::nanoseconds>(end - start).count()
            * 0.000001
        << "ms" << endl;
  }
  return 0;
}
